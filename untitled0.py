# -*- coding: utf-8 -*-
"""
Created on Tue May 11 07:15:55 2021

@author: aleks
"""

import unittest

import temp

class CalcTest(unittest.TestCase):

    def test_add(self):

        self.assertEqual(temp.add(2, 4), 6)

def test_sub(self):

    self.assertEqual(temp.sub(8, 2), 4)

def test_mul(self):

    self.assertEqual(temp.mul(5, 5), 25)
    
def test_div(self):

    self.assertEqual(temp.div(15, 3), 5)

if __name__ == '__main__':

    unittest.main()